/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcu;

/**
 *
 * @author JuliAkbal
 */
public class CALCU {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       System.out.println("==>CalculadoraInt --> resultado = " + CALCU.engine_1().calculate(5,5));
       System.out.println("==>CalculadoraInt --> resultado = " + CALCU.engine_1((int)6,(int)5).calculate(5,5));
       System.out.println("==>CalculadoraInt --> resultado = " + CALCU.engine_1((long)6,(long)5).calculate(5,5));
       System.out.println("==>CalculadoraLong --> resultado = " + CALCU.engine_2().calculate(6,2));
       //CALCU.engine((int x, int y)-> x + y);
       //CALCU.engine((long x, long y)-> x * y);
       //CALCU.engine((int x, int y)-> x - y);
       //CALCU.engine((long x, long y)-> x / y);
      // CALCU.engine((int x, int y)-> x % y);;
       
    }
     private static CalculadoraInt engine_1(){
        return (x,y) -> x * y;
    }
     private static CalculadoraInt engine_1(int a, int b){
        return (x,y) -> a * b;
    }
    private static CalculadoraLong engine_1(long a, long b){
        return (x,y) -> a + b;
    }
    private static CalculadoraLong engine_2(){
        return (x,y) -> x - y;
    }
   // private static engine(CalculadoraInt engi){
     //   int x = 2, y =4;
       // int resultado = engi.calculate(x, y);
        ///7System.out.println("resultado= "+ resultado);
        
    //}
    //private static void engine(CalculadoraLong engi){
    //    int x = 2, y =4;
    //    int resultado = (int) engi.calculate(x, y);
    //    System.out.println("resultado= "+ resultado);
    //}
   
}

