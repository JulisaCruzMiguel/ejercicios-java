/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ambiguedad;

/**
 *
 * @author JuliAkbal
 */
public class Principal {

    public static void main(String[] args) {
     
        
        Principal.engine((int x, int y)-> x + y);
        Principal.engine((long x, long y)-> x * y);
        Principal.engine((int x, int y)-> x - y);
        Principal.engine((long x, long y)-> x / y);
        Principal.engine((int x, int y)-> x % y);
       
        
    }
    private static void engine(CalculadoraInt cal){
        int x = 2, y =4;
        int resultado = cal.calcular(x, y);
        System.out.println("resultado= "+ resultado);
    }
    private static void engine(CalculadoraLong cal){
        int x = 2, y =4;
        int resultado = (int) cal.calcular(x, y);
        System.out.println("resultado= "+ resultado);
    }
}
